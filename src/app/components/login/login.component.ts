import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HotToastService } from '@ngneat/hot-toast';

import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  constructor(private authService: AuthenticationService,
              private router: Router,
              private toast: HotToastService) { }

  ngOnInit(): void {
  }

  get username() {
    return this.loginForm.get('username');
  }

  get password(){
    return this.loginForm.get('password');
  }

  submit(){
    if (!this.loginForm.valid){
      return;
    }
    const { username, password } = this.loginForm.value;
    this.authService.login(username, password).pipe(
      this.toast.observe({
        success: 'Inicio sesión con éxito',
        loading: 'Iniciando Sesión',
        error: 'Hubo un error'
        })
      ).subscribe(() =>{
      this.router.navigate(['/home']);
    });
  }



}
