// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'freddy-project-c9bd3',
    appId: '1:661390683620:web:55bac845bf1182fe182bf1',
    storageBucket: 'freddy-project-c9bd3.appspot.com',
    apiKey: 'AIzaSyC1upgbWx9RqvELFseknGRnRuJk_hiMwhI',
    authDomain: 'freddy-project-c9bd3.firebaseapp.com',
    messagingSenderId: '661390683620',
    measurementId: 'G-PES8K1E1C0',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
